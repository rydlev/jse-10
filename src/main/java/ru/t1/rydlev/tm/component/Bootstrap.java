package ru.t1.rydlev.tm.component;

import ru.t1.rydlev.tm.api.component.IBootstrap;
import ru.t1.rydlev.tm.api.controller.ICommandController;
import ru.t1.rydlev.tm.api.controller.IProjectController;
import ru.t1.rydlev.tm.api.controller.IProjectTaskController;
import ru.t1.rydlev.tm.api.controller.ITaskController;
import ru.t1.rydlev.tm.api.repository.ICommandRepository;
import ru.t1.rydlev.tm.api.repository.IProjectRepository;
import ru.t1.rydlev.tm.api.repository.ITaskRepository;
import ru.t1.rydlev.tm.api.service.ICommandService;
import ru.t1.rydlev.tm.api.service.IProjectService;
import ru.t1.rydlev.tm.api.service.IProjectTaskService;
import ru.t1.rydlev.tm.api.service.ITaskService;
import ru.t1.rydlev.tm.constant.ArgumentConstant;
import ru.t1.rydlev.tm.constant.TerminalConstant;
import ru.t1.rydlev.tm.controller.CommandController;
import ru.t1.rydlev.tm.controller.ProjectController;
import ru.t1.rydlev.tm.controller.ProjectTaskController;
import ru.t1.rydlev.tm.controller.TaskController;
import ru.t1.rydlev.tm.enumerated.Status;
import ru.t1.rydlev.tm.model.Project;
import ru.t1.rydlev.tm.repository.CommandRepository;
import ru.t1.rydlev.tm.repository.ProjectRepository;
import ru.t1.rydlev.tm.repository.TaskRepository;
import ru.t1.rydlev.tm.service.CommandService;
import ru.t1.rydlev.tm.service.ProjectService;
import ru.t1.rydlev.tm.service.ProjectTaskService;
import ru.t1.rydlev.tm.service.TaskService;
import ru.t1.rydlev.tm.util.TerminalUtil;

public final class Bootstrap implements IBootstrap {

    private final ICommandRepository commandRepository = new CommandRepository();

    private final IProjectRepository projectRepository = new ProjectRepository();

    private final ITaskRepository taskRepository = new TaskRepository();

    private final ICommandService commandService = new CommandService(commandRepository);

    private final IProjectService projectService = new ProjectService(projectRepository);

    private final ITaskService taskService = new TaskService(taskRepository);

    private final ICommandController commandController = new CommandController(commandService);

    private final IProjectTaskService projectTaskService = new ProjectTaskService(projectRepository, taskRepository);

    private final IProjectTaskController projectTaskController = new ProjectTaskController(projectTaskService);

    private final IProjectController projectController = new ProjectController(projectService, projectTaskService);

    private final ITaskController taskController = new TaskController(taskService);


    private void initDemoData() {
        projectService.add(new Project("TEST PROJECT", "TEST DESC", Status.NOT_STARTED));
        projectService.add(new Project("DEMO PROJECT", "DEMO DESC", Status.IN_PROGRESS));
        projectService.add(new Project("BEST PROJECT", "BEST DESC", Status.NOT_STARTED));
        projectService.add(new Project("BETA PROJECT", "BETA DESC", Status.COMPLETED));

        taskService.create("MEGA TASK", "MEGA DESC");
        taskService.create("VEGA TASK", "VEGA DESC");
    }

    @Override
    public void run(final String... args) {
        parseArguments(args);
        initDemoData();
        parseCommands();
    }

    private void parseArguments(final String[] args) {
        if (args == null || args.length == 0) return;
        final String arg = args[0];
        parseArgument(arg);
    }

    private void parseCommands() {
        commandController.showWelcome();
        while (!Thread.currentThread().isInterrupted()) {
            System.out.println("ENTER COMMAND:");
            final String command = TerminalUtil.nextLine();
            parseCommand(command);
        }
    }

    private void parseArgument(final String arg) {
        if (arg == null || arg.isEmpty()) return;
        switch (arg) {
            case ArgumentConstant.HELP:
                commandController.showHelp();
                break;
            case ArgumentConstant.VERSION:
                commandController.showVersion();
                break;
            case ArgumentConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case ArgumentConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case ArgumentConstant.COMMANDS:
                commandController.showCommands();
                break;
            default:
                commandController.showArgumentError();
        }
        exit();
    }

    private void parseCommand(final String command) {
        if (command == null || command.isEmpty()) return;
        switch (command) {
            case TerminalConstant.HELP:
                commandController.showHelp();
                break;
            case TerminalConstant.VERSION:
                commandController.showVersion();
                break;
            case TerminalConstant.INFO:
                commandController.showDeveloperInfo();
                break;
            case TerminalConstant.ARGUMENTS:
                commandController.showArguments();
                break;
            case TerminalConstant.COMMANDS:
                commandController.showCommands();
                break;
            case TerminalConstant.PROJECT_CREATE:
                projectController.createProject();
                break;
            case TerminalConstant.PROJECT_CLEAR:
                projectController.clearProjects();
                break;
            case TerminalConstant.PROJECT_LIST:
                projectController.showProjects();
                break;
            case TerminalConstant.PROJECT_SHOW_BY_INDEX:
                projectController.showProjectByIndex();
                break;
            case TerminalConstant.PROJECT_SHOW_BY_ID:
                projectController.showProjectById();
                break;
            case TerminalConstant.PROJECT_UPDATE_BY_INDEX:
                projectController.updateProjectByIndex();
                break;
            case TerminalConstant.PROJECT_UPDATE_BY_ID:
                projectController.updateProjectById();
                break;
            case TerminalConstant.PROJECT_REMOVE_BY_INDEX:
                projectController.removeProjectByIndex();
                break;
            case TerminalConstant.PROJECT_REMOVE_BY_ID:
                projectController.removeProjectById();
                break;
            case TerminalConstant.TASK_CREATE:
                taskController.createTask();
                break;
            case TerminalConstant.TASK_CLEAR:
                taskController.clearTasks();
                break;
            case TerminalConstant.TASK_LIST:
                taskController.showTasks();
                break;
            case TerminalConstant.TASK_SHOW_BY_INDEX:
                taskController.showTaskByIndex();
                break;
            case TerminalConstant.TASK_SHOW_BY_ID:
                taskController.showTaskById();
                break;
            case TerminalConstant.TASK_UPDATE_BY_INDEX:
                taskController.updateTaskByIndex();
                break;
            case TerminalConstant.TASK_UPDATE_BY_ID:
                taskController.updateTaskById();
                break;
            case TerminalConstant.TASK_REMOVE_BY_INDEX:
                taskController.removeTaskByIndex();
                break;
            case TerminalConstant.TASK_REMOVE_BY_ID:
                taskController.removeTaskById();
                break;
            case TerminalConstant.TASK_BIND_TO_PROJECT:
                projectTaskController.bindTaskToProject();
                break;
            case TerminalConstant.TASK_UNBIND_FROM_PROJECT:
                projectTaskController.unbindTaskFromProject();
                break;
            case TerminalConstant.TASK_SHOW_BY_PROJECT_ID:
                taskController.showTaskByProjectId();
                break;
            case TerminalConstant.PROJECT_START_BY_INDEX:
                projectController.startProjectByIndex();
                break;
            case TerminalConstant.PROJECT_START_BY_ID:
                projectController.startProjectById();
                break;
            case TerminalConstant.PROJECT_COMPLETE_BY_INDEX:
                projectController.completeProjectByIndex();
                break;
            case TerminalConstant.PROJECT_COMPLETE_BY_ID:
                projectController.completeProjectById();
                break;
            case TerminalConstant.PROJECT_CHANGE_STATUS_BY_INDEX:
                projectController.changeProjectStatusByIndex();
                break;
            case TerminalConstant.PROJECT_CHANGE_STATUS_BY_ID:
                projectController.changeProjectStatusById();
                break;
            case TerminalConstant.TASK_START_BY_INDEX:
                taskController.startTaskByIndex();
                break;
            case TerminalConstant.TASK_START_BY_ID:
                taskController.startTaskById();
                break;
            case TerminalConstant.TASK_COMPLETE_BY_INDEX:
                taskController.completeTaskByIndex();
                break;
            case TerminalConstant.TASK_COMPLETE_BY_ID:
                taskController.completeTaskById();
                break;
            case TerminalConstant.TASK_CHANGE_STATUS_BY_INDEX:
                taskController.changeTaskStatusByIndex();
                break;
            case TerminalConstant.TASK_CHANGE_STATUS_BY_ID:
                taskController.changeTaskStatusById();
                break;
            case TerminalConstant.EXIT:
                exit();
                break;
            default:
                commandController.showCommandError();
        }
    }

    private void exit() {
        System.exit(0);
    }

}
