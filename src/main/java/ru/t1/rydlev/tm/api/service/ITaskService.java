package ru.t1.rydlev.tm.api.service;

import ru.t1.rydlev.tm.api.repository.ITaskRepository;
import ru.t1.rydlev.tm.enumerated.Status;
import ru.t1.rydlev.tm.model.Task;

import java.util.List;

public interface ITaskService extends ITaskRepository {

    Task create(String name);

    Task create(String name, String description);

    Task updateById(String id, String name, String description);

    Task updateByIndex(Integer index, String name, String description);

    List<Task> findAllByProjectId(String projectId);

    Task changeTaskStatusById(String id, Status status);

    Task changeTaskStatusByIndex(Integer index, Status status);

}
